
import torch


def spearman_v2(x: torch.Tensor, y: torch.Tensor):
    n = x.size(0)
    order_x = torch.argsort(x)
    rank_x = torch.empty(order_x.size(0), device=order_x.device, dtype=torch.float32)
    rank_x[order_x] = torch.arange(order_x.size(0), device=order_x.device, dtype=torch.float32)

    order_y = torch.argsort(y)
    rank_y = torch.empty(order_y.size(0), device=order_y.device, dtype=torch.float32)
    rank_y[order_y] = torch.arange(order_y.size(0), device=order_y.device, dtype=torch.float32)

    spearman = 1 - 6 * torch.sum((rank_x - rank_y) ** 2) / (n * (n ** 2 - 1))
    return spearman


def spearman_v1(x: torch.Tensor, y: torch.Tensor):
    order_x = torch.argsort(x)
    rank_x = torch.empty(order_x.size(0), device=order_x.device, dtype=torch.float32)
    rank_x[order_x] = torch.arange(order_x.size(0), device=order_x.device, dtype=torch.float32)

    order_y = torch.argsort(y)
    rank_y = torch.empty(order_y.size(0), device=order_y.device, dtype=torch.float32)
    rank_y[order_y] = torch.arange(order_y.size(0), device=order_y.device, dtype=torch.float32)

    return pearson(rank_x, rank_y)


def pearson(x: torch.Tensor, y: torch.Tensor):
    mean_x = torch.mean(x)
    mean_y = torch.mean(y)

    cov_xy = torch.mean((x - mean_x) * (y - mean_y))
    std_x = torch.sqrt(torch.mean((x - mean_x) ** 2))
    std_y = torch.sqrt(torch.mean((y - mean_y) ** 2))

    return cov_xy / (std_x * std_y)
