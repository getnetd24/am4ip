
import torch
from abc import ABC, abstractmethod


def calc_psnr(img, img_ref):
    return 10. * torch.log10(255 ** 2 / torch.mean(torch.square(img - img_ref)))


def gen_grad_filter(dtype=torch.float32, device="cuda"):
    grad_filters = torch.tensor([[1/3, 0, -1/3],
                                 [1/3, 0, -1/3],
                                 [1/3, 0, -1/3]], dtype=dtype, device=device)  # only G_x
    grad_filters = torch.concat([grad_filters[None, None, ...], grad_filters.T[None, None, ...]], dim=0)  # 2 x 1 x 3 x 3

    return grad_filters


def calc_gmsd(img, img_ref, c=0.0026):
    # 1. compute gradients
    images = torch.concat([img, img_ref], dim=0)  # shape = 2 x 1 x H x W
    grad_filter = gen_grad_filter(img.dtype, img.device)
    grad = torch.conv2d(images, grad_filter, padding="valid")  # 2 x 2 x H x W

    # 2. compute GM:
    gm = torch.sqrt(torch.sum(grad**2, dim=1))  # 2 x H x W

    # 3. compute luminance sim term:
    lsim = (2 * gm[0] * gm[1] + c) / (gm[0]**2 + gm[1]**2 + c)  # H x W

    # 4. mean pooling
    mean_lsim = torch.mean(lsim)

    # 5. cov pooling
    cov_lsim = torch.sqrt(torch.mean((lsim - mean_lsim)**2))

    return cov_lsim


def calc_ssim(I, I_ref, r=5, sigma=1.5, max_value=255., k_1=0.01, k_2=0.03):  # I.size() = I_ref.size() = (1, C, H, W)
    images = torch.concat([I, I_ref], dim=0)  # shape = 2 x C x H x W
    gaussian_filter_1d = torch.exp(-0.5*(torch.arange(-r, r+1, device=I.device, dtype=I.dtype)/sigma)**2)

    # torch implemented conv2d using groups, so syntax is a bit harder: we have 3 inputs and need 1 convolution applied to each input.
    # => group = channel number (3)
    # => output / group number = 1
    gaussian_filter_2d = gaussian_filter_1d.view(-1, 1) * gaussian_filter_1d.view(1, -1)
    gaussian_filter_2d = (gaussian_filter_2d / gaussian_filter_2d.sum()).view(1, 1, 2*r+1, 2*r+1).repeat((I.size(1), 1, 1, 1))

    c_1 = (k_1 * max_value) ** 2
    c_2 = (k_2 * max_value) ** 2

    mu = torch.conv2d(images, gaussian_filter_2d, padding="valid", groups=I.size(1))  # 2 x 3 x H x W
    var = torch.conv2d(images**2, gaussian_filter_2d, padding="valid", groups=I.size(1)) - mu**2  # 2 x 3 x H x W
    cross_var = torch.conv2d((images[0] * images[1]).unsqueeze(0), gaussian_filter_2d, padding="valid", groups=I.size(1)).squeeze(0) - mu[0] * mu[1]  # 3 x H x W

    luminance = (2. * mu[0] * mu[1] + c_1) / (mu[0]**2 + mu[1]**2 + c_1)   # 3 x H x W
    structure = (2. * cross_var + c_2) / (var[0] + var[1] + c_2)

    ssim = torch.mean(luminance * structure)
    return ssim


def calc_tv(img):
    # 1. compute gradients
    grad_filters = torch.tensor([[0, 0, 0], [0, -1, 1], [0, 0, 0]], dtype=img.dtype, device=img.device)  # only G_x. Made it 3x3 instead of 1x3 to compute G_y simultaneously
    grad_filters = torch.concat([grad_filters[None, None, ...], grad_filters.T[None, None, ...]], dim=0)  # 2 x 1 x 3 x 3

    grad = torch.conv2d(img, grad_filters, padding="valid")  # 1 x 2 x H x W

    # 2. compute GM:
    gm = torch.sqrt(torch.sum(grad**2, dim=1))  # 1 x H x W

    return torch.sum(gm, dim=[-1, -2])


class IQAMetric(ABC):
    def __init__(self, name: str):
        self.name = name

    @abstractmethod
    def __call__(self, batch_img: torch.Tensor, batch_img_ref: torch.Tensor) -> torch.Tensor:
        """Compute an IQA metric on a batch of images, each batch being composed of N distorted images in `batch_img` for a given reference in `batch_img_ref`.

        :param batch_img: Tensor with shape B x N x C x H x W. Contains B batches of N images with (H,W) spatial resolution and C channels.
        :param batch_img_ref: Tensor with shape B x 1 x C x H x W. Contains B batches of 1 reference image with (H,W) spatial resolution and C channels.
        :return: Tensor with shape B x N. Contains the resulting metric for each pair of distorted image x reference image.
        """
        raise NotImplementedError


class PSNR(IQAMetric):
    def __init__(self):
        super().__init__(name="PSNR")

    def __call__(self, batch_img: torch.Tensor, batch_img_ref: torch.Tensor) -> torch.Tensor:
        # # Images must of of channel == 1 (gray level images):
        # if batch_img.size(2) != 1 and batch_img_ref.size(2) != 1:
        #     raise ValueError("Images have too many channels. Use `PSNRc` for color PSNR. "
        #                      f"Shapes: {batch_img.size}  | {batch_img_ref.size}.")

        return 10. * torch.log10(255. ** 2 / torch.mean(torch.square(batch_img - batch_img_ref), dim=[-1, -2, -3]))
